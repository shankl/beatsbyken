**BeatsByKen**



Client: Ken Abrams

Team: AIdan Carroll, Alex Calamaro, Max Willard, Liz Shank


This app will be used to track the heart rate of participants in a psychology research study conducted by Professor Ken Abrams and his students.
Six participants will be wearing Polar H7 heart rate monitors, and the app (placed on six different iPads) will a) track the number
of heartbeats that participants experience across fixed time periods, and b) export the resulting data in the form of a .csv spreadsheet to Dropbox. 
The heart rate monitors send data via Bluetooth. Importantly, the app will be able to "lock in" to a particular heart rate monitor rather than grab data from 
the strongest signal it detects.

Currently the app is incomplete although much of the core functionality is implemented. Users can select a specific heart rate monitor to connect to and the 
connection will persist as long as the heart rate monitor remains on and in range. If the connection is broken a warning will appear.

The app also able to count heart beats for each trial, and then export data to Dropbox.


Still to Do:

The UI needs to be updated. We focused on functionality over UI.

More extensive testing.

History is not implemented, including deleting local data

SummaryView (after a trial) is incomplete.



