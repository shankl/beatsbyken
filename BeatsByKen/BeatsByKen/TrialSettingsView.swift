//
//  TrialSettingsView.swift
//  BeatsByKen
//
/*Copyright (c) 2015 Aidan Carroll, Alex Calamaro, Max Willard, Liz Shank

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
// Code modified from original at:
// http://www.raywenderlich.com/78550/beginning-ios-collection-views-swift-part-1
// http://www.raywenderlich.com/78550/beginning-ios-collection-views-swift-part-2

import Foundation
import UIKit
import CoreData

class TrialSettingsView : UICollectionViewController, SelectDurationVCDelegate, UIPopoverPresentationControllerDelegate {
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let settingsFields = ["Pre-1", "Pre-2", "Pre-3", "Pre-4", "Post-1", "Post-2", "Post-3", "Post-4", "Exer-1", "Exer-2", "Exer-3", "Exer-4"]
    private let reuseIdentifier = "TrialSettingsCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)

    var settings = [NSManagedObject]()
    var strSaveText : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // If no trial settings have been saved, create default settings with duration of 30 seconds
        for (index,field) in enumerate(settingsFields) {
            if (!SessionTable.checkInManagedObjectContext(managedObjectContext!, participantID: trialSettingsPID, sessionID: trialSettingsSID, tableViewRow: index)) {
                let newSettings = SessionTable.createInManagedObjectContext(managedObjectContext!, heartbeatCount: 0, trialDuration: 30, trialType: field, participantID: trialSettingsPID, sessionID: trialSettingsSID, tableViewRow: index)
                println(newSettings)
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadTrialSettings()
    }
    
    // load the current settings into the view
    func loadTrialSettings() {
        if let temp = SessionTable.getCurrentSettings(managedObjectContext!, participantID: trialSettingsPID, sessionID: trialSettingsSID) {
            settings = temp
        }
        else {
            println("no settings...")
        }

    }
    
    // on select, pop over duration pickerview
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
            let setting = settings[indexPath.row]
            
            var durationString = ""
            if let v = setting.valueForKey("trialDuration") as? Int {
                durationString = "\(v)"
            }
            
            var trialType = setting.valueForKey("trialType") as? String
            var x = "\(indexPath.row)"
            let y = x.toInt()

            selectDuration(cell, trialType: trialType, trialDuration: durationString, trialIndex: y!)
        } else {
            // Error indexPath is not on screen: this should never happen.
        }
        
    }

    // initialize pickerview
    func selectDuration(sender: UIView!, trialType: String?, trialDuration : String, trialIndex: Int) {
        let durationPickerView = storyboard?.instantiateViewControllerWithIdentifier("SelectDurationView") as! SelectDurationView
        
        
        durationPickerView.delegate=self;
        durationPickerView.selectedTrialType=trialType!;
        durationPickerView.selectedTrialDuration=trialDuration;
        durationPickerView.selectedTrialIndex=trialIndex;
        
        durationPickerView.modalPresentationStyle = .Popover
        if let popoverController = durationPickerView.popoverPresentationController {
            popoverController.sourceView = sender!
            popoverController.sourceRect = sender!.bounds
            popoverController.permittedArrowDirections = .Any
            popoverController.delegate = self
        }
        presentViewController(durationPickerView, animated: true, completion: nil)
        
    }
    
    // handle reload after duration pickerview
    func saveText(trialType: String, trialDuration: String, trialIndex: Int) {
        
        println(trialType)
        println(trialDuration)
        let durationVal = trialDuration.toInt()
        println(durationVal! + 5)
        SessionTable.updateCurrentSetting(managedObjectContext!, heartbeatCount: 0, trialDuration: durationVal!, trialType: trialType, participantID: trialSettingsPID, sessionID: trialSettingsSID, tableViewRow: trialIndex)
        self.collectionView!.reloadData()
        loadTrialSettings()
        
    }
    
    // MARK: - UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .FullScreen
    }
    
    func presentationController(controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TrialSettingsView : UICollectionViewDataSource {
    //1
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    //2
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settings.count
    }
    
    //3
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //1
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! TrialSettingsCell

        let setting = settings[indexPath.row]

        var str = ""
        if let v = setting.valueForKey("trialDuration") as? Int {
            str = "\(v)"
        }
        
        cell.trialTypeLabel!.text = setting.valueForKey("trialType") as? String
        cell.trialDurationLabel!.text = str
        
        return cell
    }
}

extension TrialSettingsView : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: 100, height: 200)
    }
    
    //3
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
}


